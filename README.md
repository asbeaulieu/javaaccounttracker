# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Sample of a simple, terminal command based, tracker application for bank accounts.
* 1.0

### How do I get set up? ###

* Created in Java 7
* Unknown backwards compatibility with previous Java versions. Recommended to update to Java 7
* Errors removed and ready to compile
* `javac ApplicationTracker.java`
* `java ApplicationTracker`


### Who do I talk to? ###

* Author: Andrew Beaulieu
* Date: Feb 2016